import { Kafka } from 'svc-common-messaging';
import * as pkg from 'package.json';

const client = new Kafka({
  clientId: 'svc-bgg',
  brokers: ['0.0.0.0:9092'],
});

export const producer: any = client.createProducer();
export const consumer: any = client.createConsumer({ groupId: `${pkg.name}-group` });
export default client;
