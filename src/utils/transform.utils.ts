import * as _ from 'lodash';
import { Logger } from '@nestjs/common';

/**
 * An object used to get, transform, or change the resulting key
 * for a given parameter or payload property
 * @property path - path to the value
 * @property key - name of key in the final object
 * @property transform - transformer for changing input value to a desired value
 */
interface ISchemaObject {
  key: ((rawObj: object) => string) | string;
  path?: string;
  preserveEmptyValue?: boolean | undefined;
  transform?: (rawObj: any) => string | number | null;
}

export type Schema = Array<(string | ISchemaObject)>;

export interface ServiceHandlerParams {
  [key: string]: string | number | null | object | Array<(string | number | null | undefined)>;
}

export function mapToSchema(schema: Schema, input: ServiceHandlerParams): ServiceHandlerParams {
  const payload: ServiceHandlerParams = {};

  // tslint:disable-next-line:prefer-for-of
  for (let i = 0; i < schema.length; i += 1) {
    const config: any = _.isString(schema[i]) ? { path: schema[i] } : schema[i];

    // key in the final payload
    let key = config.path;
    if (config.key) {
      key = typeof config.key === 'function'
        ? config.key(input)
        : config.key;
    }

    Logger.debug(`Config Key: ${key}`);

    // final value, transformed or not
    const value = typeof config.transform === 'function'
      ? config.transform(input)
      : input[config.path];

    // gives us a way to omit undefineds from final params
    if (config.preserveEmptyValue || !_.isUndefined(value)) {
      payload[key] = value;
    }
  }

  return payload;
}
