import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ScheduleModule } from './schedule/schedule.module';
import { producer } from './config/kafka.config';

@Module({
  imports: [ScheduleModule],
  controllers: [AppController],
  providers: [AppService],
})

export class AppModule {
  constructor() {
    this.init();
  }

  async init() {
    // kafka connections
    await producer.connect();
  }
}
