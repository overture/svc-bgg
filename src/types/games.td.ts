
export interface IShallowGame {
  bgg_id: string;
  bgg_url: string;
  bgg_slug: string;
  name: string;
}
