import { Module } from '@nestjs/common';
import { BggService } from './bgg.service';
import { BggClientModule } from 'src/bgg-client/bgg-client.module';
import { RedisConnectionProvider } from 'src/libs/redis.lib.provider';

@Module({
  imports: [BggClientModule],
  providers: [BggService, RedisConnectionProvider],
  exports: [BggService],
})
export class BggModule {}
