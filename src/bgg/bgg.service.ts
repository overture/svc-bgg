import { Injectable, Inject, Logger } from '@nestjs/common';
import { filter as asyncFilter, queue } from 'async';
import { random as randomNumber } from 'lodash';
import { createMessage, Topics, GAME_CREATED_BULK } from 'svc-common-messaging';

import { producer } from '../config/kafka.config';
import { gameDetailsSchema } from './schemas/bgg.game.schemas';
import { mapToSchema } from 'src/utils/transform.utils';
import { BggClientService } from 'src/bgg-client/bgg-client.service';
import { BggClientRepository } from 'src/bgg-client/bgg-client.repository';
import { RedisClient } from 'redis';
import { writeFileSync } from 'fs';

const QUEUE_CONCURRENCY = 2;
const QUEUE_FINISH_DELAY = 5000;
const REQUEST_DELAY = randomNumber(1, 3) * 1000;

@Injectable()
export class BggService {
  private batchGames: any[] = [];

  constructor(
    @Inject('REDIS_CONNECTION') private readonly cacheClient: RedisClient,
    private readonly bggClient: BggClientService,
    private readonly bggRepository: BggClientRepository,
  ) {}

  /**
   * Starts the ingest process
   * @param page `number`
   */
  public performIngest(page) {
    return new Promise(async (resolve) => {
      const shallowGamesData = await this.bggClient.getPageOfGamesAsList(page);
      const missingGames = await this.filterExistingGames(shallowGamesData);

      if (missingGames) {
        const q = queue(this.setupDetailQueue.bind(this), QUEUE_CONCURRENCY);

        q.drain(() => {
          setTimeout(() => {
            this.onDetailsQueueComplete();
            resolve(this.batchGames);
          }, QUEUE_FINISH_DELAY);
        });
        q.push(missingGames, this.onDetailsQueueTaskComplete.bind(this));
      }
    });
  }

  private async filterExistingGames(shallowGames) {
    const filtered = await asyncFilter(shallowGames, (game, cb) => {
      this.cacheClient.sismember('incomingGames', game.bgg_id, (error, gameExists) => {
        return !gameExists ? cb(null, true) : cb(null, false);
      });
    });

    return filtered;
  }

  private setupDetailQueue({ bgg_id }, callback) {
    setTimeout(async () => {
      try {
        const result = await this.bggRepository.getFullGameDetails(bgg_id).then(this.prepGameResponse);
        this.batchGames.push(result);
        this.cacheClient.sadd('incomingGames', bgg_id);
        callback(null, bgg_id, result);
      } catch (error) {
        Logger.error(error, error.stack, 'setupDetailQueue');
      }
    }, REQUEST_DELAY);
  }

  private onDetailsQueueTaskComplete(error, game) {
    Logger.debug(`Proceessed: ${game}: Batch Length ${this.batchGames.length}`);

    if (error) {
      Logger.error(`Error while processing: ${game}`);
      Logger.error(error);
    }
  }

  private onDetailsQueueComplete() {
    const batchGames = this.batchGames;
    Logger.log(`Page job finished successfully with ${this.batchGames.length} games`);

    if (batchGames.length) {
      producer.send({
        topic: Topics.WEBCONSOLE,
        messages: [createMessage({
          type: GAME_CREATED_BULK,
          data: batchGames,
        })],
      });
    }

    this.batchGames = [];
  }

  private prepGameResponse(resp) {
    const game = resp.items.item;
    return new Promise((resolve, reject) => {
      if (!game) {
        return reject();
      }

      let gameData;
      try {
        gameData = mapToSchema(gameDetailsSchema, game);
      } catch (error) {
        return reject(error.toString());
      }
      return resolve(gameData);
    });
  }
}
