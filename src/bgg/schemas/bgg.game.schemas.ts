import { Schema } from 'src/utils/transform.utils';

export const gameDetailsSchema: Schema = [
  'description',
  'image',
  {
    key: 'categories',
    transform: ({ link }) => link.filter(
      ({ type }) => type === 'boardgamecategory',
    ).map(
      ({ id, value }) => ({ id, name: value }),
    ),
  },
  {
    path: 'id',
    key: 'bggId',
  },
  {
    key: 'name',
    transform: ({ name }) => {
      if (name instanceof Array) {
        return name.filter(n => n.type === 'primary');
      }

      return name.primary;
    },
  },
  {
    path: 'yearpublished.value',
    key: 'yearPublished',
  },
  {
    key: 'publishers',
    transform: ({ link }) => link.filter(
      ({ type }) => type === 'boardgamepublisher',
    ).map(
      ({ id, value }) => ({ id, name: value }),
    ),
  },
  {
    path: 'minplayers.value',
    key: 'playerMin',
  },
  {
    path: 'maxplayers.value',
    key: 'playerMax',
  },
  {
    key: 'playerBest',
    transform: ({ poll }) => {
      const suggestedPlayerCounts = poll.find(
        ({ name }) => name === 'suggested_numplayers',
      );

      if (!suggestedPlayerCounts) {
        return null;
      }

      return suggestedPlayerCounts.results.reduce((prev, current) => {
        return (
          prev.watermark > current.result[0].best
            ? prev
            : {
              playerNum: current.numplayers,
              watermark: current.result[0].best,
            }
        );
      }, {
        playerNum: 0,
        watermark: 0,
      })
      .playerNum;
    },
  },
  {
    path: 'minplaytime.value',
    key: 'durationMin',
  },
  {
    path: 'maxplaytime.value',
    key: 'durationMax',
  },
  {
    key: 'mechanics',
    transform: ({ link }) => link.filter(
      ({ type }) => type === 'boardgamemechanic',
    ).map(
      ({ id, value }) => ({ id, name: value }),
    ),
  },
  {
    key: 'isExpansion',
    transform: ({ link }) => {
      const hasParent = link.filter(
        ({ type }) => type === 'boardgameexpansion',
      ).find(
        ({ inbound }) => inbound === true,
      );

      return Boolean(hasParent);
    },
  },
  {
    key: 'expansions',
    transform: ({ link }) => {
      const expansions = link.filter(
        ({ type }) => type === 'boardgameexpansion',
      );
      const hasParent = expansions.find(
        ({ inbound }) => inbound === true,
      );

      return hasParent ? [] : expansions.map(
        ({ id, value }) => ({ id, name: value }),
      );
    },
  },
  {
    key: 'bggRank',
    transform: ({ statistics }) => {
      const rankItem = statistics.ratings.ranks.rank.find(rank => rank.name === 'boardgame');
      return rankItem ? rankItem.value : -1;
    },
  },
  {
    path: 'statistics.ratings.averageweight.value',
    key: 'weight',
  },
];
