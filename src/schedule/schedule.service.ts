import { range } from 'lodash';
import { queue } from 'async';
import { Injectable, Logger } from '@nestjs/common';
import { NestSchedule, Cron, Interval } from 'nest-schedule';
import { BggClientService } from 'src/bgg-client/bgg-client.service';
import { BggService } from 'src/bgg/bgg.service';

const SCRAPE_INTERVAL = Number(process.env.SCRAPE_INTERVAL) || 4000;

@Injectable()
export class ScheduleService extends NestSchedule {
  constructor(
    private readonly bggClient: BggClientService,
    private readonly bggService: BggService,
  ) {
    super();
  }

  // 1am every Monday
  @Cron('0 1 * * 1', {
    startTime: new Date(),
  })
  async scheduleBggIngest() {
    const total = await this.bggClient.getTotalPageCount();
    const pageTasks = range(1, total + 1);
    const q = queue(this.queueSetup.bind(this), 1);

    q.drain(this.onQueueComplete);
    q.error(this.onQueueError);
    q.push(pageTasks, this.onQueueTaskComplete);

    Logger.debug(`Scheduled Board Game Geek ingest has started with ${total} pages`);
  }

  queueSetup(task, callback) {
    setTimeout(async () => {
      const result = await this.bggService.performIngest(task);
      callback(null, task, result);
    }, SCRAPE_INTERVAL);
  }

  onQueueComplete() {
    Logger.log('All pages scraped sucessfully', 'ScheduleService');
  }

  onQueueError(error, task) {
    Logger.error(`Error detected in task ${task}`);
    throw new Error(error);
  }

  onQueueTaskComplete(error, task, result) {
    Logger.debug(`Finished processing page: ${task}`);
  }
}
