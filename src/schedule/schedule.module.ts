import { Module } from '@nestjs/common';

import { ScheduleService } from './schedule.service';
import { BggModule } from 'src/bgg/bgg.module';
// import { BggService } from 'src/bgg/bgg.service';
import { BggClientModule } from 'src/bgg-client/bgg-client.module';

@Module({
  imports: [BggModule, BggClientModule],
  providers: [ScheduleService],
})
export class ScheduleModule {
  constructor(
    private readonly scheduleService: ScheduleService,
  ) {
    this.scheduleService.scheduleBggIngest();
  }
}
