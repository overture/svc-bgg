import { createClient, RedisClient, print } from 'redis';

const Redis: RedisClient = createClient();

Redis.on('error', print);

export const RedisConnectionProvider = {
  provide: 'REDIS_CONNECTION',
  useValue: Redis,
};
