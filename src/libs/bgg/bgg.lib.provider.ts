import * as bgg from 'bgg';

export const BGGAPIProvider = {
  provide: 'BGG_API',
  useValue: bgg({
    retry: {
      initial: 100,
      multiplier: 2,
      max: 15e3,
    },
  }),
};
