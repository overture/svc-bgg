import { Injectable, HttpService, HttpException, Logger } from '@nestjs/common';
import * as Cheerio from 'cheerio';
import {
  last as _last,
  uniqBy as _uniqBy,
} from 'lodash';

import { IShallowGame } from 'src/types/games.td';

const baseurl = 'https://boardgamegeek.com/';

@Injectable()
export class BggClientService {
  constructor(
    private readonly httpService: HttpService,
  ) {}

  async getPageOfGamesAsList(page: number) {
    const response = await this.httpService.get(`${baseurl}/browse/boardgame/page/${page}`).toPromise();
    const $ = Cheerio.load(response.data);
    const $rows = $('table#collectionitems #row_');

    return _uniqBy((
      Object
        .keys($rows)
        .map(this.extractShallowGameData($, $rows))
        .filter(game => game !== false)
    ), 'bgg_id');
  }

  /**
   * @description Get total pages games on Board Game Geek
   * @returns Total page count (int)
   */
  async getTotalPageCount() {
    try {
      const response = await this.httpService.get(`${baseurl}/browse/boardgame/page/1`).toPromise();
      const $ = Cheerio.load(response.data);
      const $anchor = $('#main_content a[title="last page"]');
      const href = $anchor.attr('href');

      // example: /browse/boardgame/page/{{1041}}
      return Number(
        _last(href.split('/')),
      );
    } catch (error) {
      Logger.error(error.message);
    }
  }

  /**
   * @description Extracts game properties from a given page using Cheerio(jquery), returning them as an array of shallow game objects
   * @param $ Cheerio representation of the html
   * @param $rows Cheerio selected table row elements
   */
  private extractShallowGameData($, $rows) {
    return (index): IShallowGame | boolean => {
      try {
        const $game = $($rows[index]).find('.collection_objectname a');
        const href = $game.attr('href');

        return {
          bgg_id: href.split('/')[2],
          bgg_url: href,
          bgg_slug: href.split('/')[3],
          name: $game.text(),
        };
      } catch (error) {
        return false;
      }
    };
  }
}
