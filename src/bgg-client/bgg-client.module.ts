import { Module, HttpModule } from '@nestjs/common';
import { BggClientService } from './bgg-client.service';
import { BggClientRepository } from './bgg-client.repository';
import { BGGAPIProvider } from 'src/libs/bgg/bgg.lib.provider';

@Module({
  imports: [HttpModule],
  providers: [BGGAPIProvider, BggClientService, BggClientRepository],
  exports: [BGGAPIProvider, BggClientService, BggClientRepository],
})
export class BggClientModule {}
