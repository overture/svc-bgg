import { Injectable, Inject } from '@nestjs/common';

@Injectable()
export class BggClientRepository {
  @Inject('BGG_API')
  private readonly bgg: any;

  public getFullGameDetails(id) {
    return (
      this.bgg('thing', {
        id,
        type: 'boardgame,boardgameexpansion',
        stats: 1,
        videos: 1,
      })
    );
  }
}
