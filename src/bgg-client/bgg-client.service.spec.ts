import { Test, TestingModule } from '@nestjs/testing';
import { BggClientService } from './bgg-client.service';

describe('BggClientService', () => {
  let service: BggClientService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BggClientService],
    }).compile();

    service = module.get<BggClientService>(BggClientService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
